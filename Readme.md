# Hommien jako:

## Lassi L
- Task luonti
- TaskInfo
- Muistiinpano dialog
- UI (Kind of)

## Henrik:
- UI hommia

## Tuomas:
### Kamera
- Kuvanotto sisäänrakennetulla kameralla
- Kuvan tallennus
- Kuva referenssin liitto kantaan
- Kuvan Avaaminen
### Äänite
- Äänen tallennus
- Referenssin luonti äänitteestä kantaan
- Äänitteen kuunteleminen

### Datan tallennus kantaan ja sen vaativat rajapinnat
- Modelit
- Daot
- ViewModelit
- Adapterit

### UI asettelua
- Completedtasks
- TaskPage
- Yleistä hiomista ja asettelua kaikissa näkymissä


# Puuttuu

## GPS
- Todettiin että ei ole tässä projektissa tarpeellinen ominaisuus (Ainakaan vielä)
- Saattaa sekottaa koodia ja kantaa liikaa

# Ominaisuuksia

## Data tallennetaan paikalliseen SQLite kantaan käyttäen Room Persistence Kirjastoa
## Mahdollista Luoda tehtäviä
- Asettaa niille nimi ja kuvaus
- Liittää muistiinpanoja
    - Kuvia
    - Äänite
    - Teksti
## Mahdollista keskeyttää ja jatkaa tehtävää myöhemmin

## Mahdollista tarkastella tehtävä kohtaisia infoja
- Kuvaus
- Muistiinpanoja
- Kestoa
- Luomispäivä


