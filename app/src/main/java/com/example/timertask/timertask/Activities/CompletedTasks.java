package com.example.timertask.timertask.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.example.timertask.timertask.Adapters.TaskRecyclerViewAdapter;
import com.example.timertask.timertask.R;
import com.example.timertask.timertask.ViewModels.TaskViewModel;
import com.example.timertask.timertask.Models.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Show details of each task item
 * TODO: Make it possible to view/edit task
 * TODO: Show pictures, add/view/remove pictures
 */

public class CompletedTasks extends AppCompatActivity implements View.OnLongClickListener, AdapterView.OnItemLongClickListener {
    // Recycler element
    private RecyclerView taskList;
    // Our asnyc interface to the database
    private TaskViewModel taskViewModel;
    // Our RecyclerView Adapter
    private TaskRecyclerViewAdapter recyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_tasks);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Suoritetut taskit");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        // get reference to our listview
        taskList = findViewById(R.id.taskRecycleList);
        fillRecycleView();

    }
    /**
     * Top action  bar handling
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // if the toolbar arrow is pressed, return to the frontpage
        if(item.getItemId() == android.R.id.home) {
            startActivity(new Intent(CompletedTasks.this, FrontPage.class));
        }
        return true;
    }

    /**
     * Fills the view with the tasks
     */
    public void fillRecycleView() {
        // get a reference to the view model
        taskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        // create the adapter
        recyclerViewAdapter = new TaskRecyclerViewAdapter(new ArrayList<Task>(), this, this.getClass().getSimpleName());
        // Set the manager that handles displaying our elements inside the view
        taskList.setLayoutManager(new LinearLayoutManager(this));
        // add our own adapter to the recycle view so it understands our Task class
        taskList.setAdapter(recyclerViewAdapter);
        /**
         * get all tasks, and use observer pattern to update if the data updates (most likely wont happen)
          */
        taskViewModel.getFinishedTasks().observe(CompletedTasks.this, new Observer<List<Task>>() {
            @Override
            public void onChanged(@Nullable List<Task> tasks) {
                recyclerViewAdapter.addItems(tasks);
            }
        });
    }

    // When item in the recycler view is clicked, start the task?
    @Override
    public boolean onLongClick(View v) {
        // get the object connected to the item
        Task task = (Task) v.getTag();
        // get reference to the taskPage
        Intent taskInfoIntent = new Intent(this, TaskInfoActivity.class);
        taskInfoIntent.putExtra("Activity", "CompletedTasks");
        // pass the selected task object to it
        taskInfoIntent.putExtra("TASK", task);
        // open activity
        startActivity(taskInfoIntent);
        return true;
    }

    /**
     * Open task when long clicked
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     * @return true
     */
    public boolean onItemLongClick (AdapterView parent, View view, int position,long id){
        Task task = (Task) view.getTag();
        return true;
    }
}
