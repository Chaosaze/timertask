package com.example.timertask.timertask.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.timertask.timertask.Adapters.TaskRecyclerViewAdapter;
import com.example.timertask.timertask.R;
import com.example.timertask.timertask.ViewModels.TaskViewModel;
import com.example.timertask.timertask.Models.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Main menu and base view for front page
 */
public class FrontPage extends AppCompatActivity implements View.OnLongClickListener{
    // hamburger menu items
    private DrawerLayout frontPageDrawerLayout;
    private NavigationView nav;

    // Recycler element
    private RecyclerView taskList;
    // Our asnyc interface to the database
    private TaskViewModel taskViewModel;
    // Our RecyclerView Adapter
    private TaskRecyclerViewAdapter recyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.front_page_drawer);
        // get references to our items in the layout
        frontPageDrawerLayout = findViewById(R.id.front_drawer_layout);
        // Get the nav bar
        nav = findViewById(R.id.nav_view);
        // get the task list
        taskList =  findViewById(R.id.taskRecycleList);
        /*
         *  Toolbar setup
         */
        Toolbar mToolbar = findViewById(R.id.topbar);
        setSupportActionBar(mToolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_action_name);


        // Here we create the menu and add listeners to the menu items
        //Intents for different views
        final Intent luoTaskIntent = new Intent(this, LuoTask.class);
        final Intent suoritetutIntent = new Intent(this, CompletedTasks.class);
        //Menu item click listener
        nav.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        frontPageDrawerLayout.closeDrawers();
                        switch (menuItem.getItemId()) {
                            case R.id.nav_luotask:
                                // open create task view
                                startActivity(luoTaskIntent);
                                break;
                            case R.id.nav_suoritetut:
                                // open completed tasks view
                                suoritetutIntent.putExtra("Activity", "FrontPage");
                                startActivity(suoritetutIntent);
                                break;
                            case R.id.nav_exit:
                                // close process
                                android.os.Process.killProcess(android.os.Process.myPid());
                                break;
                        }
                        return true;
                    }
        });
        addOnClickListeners();
        fillRecycleView();
    }
    /**
     * Attach onclicklisteners to buttons
     */
    public void addOnClickListeners() {
        Button newTaskButton;
        newTaskButton =  findViewById(R.id.newTask);
        // open the create task activity
        newTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FrontPage.this, LuoTask.class);
                startActivity(intent);
            }
        });
    }
    /**
     * Fills the recyclerview with data from our viewmodel
     */
    public void fillRecycleView() {
        // get a reference to the view model
        taskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        // create the adapter
        recyclerViewAdapter = new TaskRecyclerViewAdapter(new ArrayList<Task>(), this, this.getClass().getSimpleName());
        // Set the manager that handles displaying our elements inside the view
        taskList.setLayoutManager(new LinearLayoutManager(this));
        // add our own adapter to the recycle view so it understands our Task class
        taskList.setAdapter(recyclerViewAdapter);
        // get all tasks, and use observer pattern to update if the data updates (most likely wont happen)
        taskViewModel.getUnFinishedTasks().observe(FrontPage.this, new Observer<List<Task>>() {
            @Override
            public void onChanged(@Nullable List<Task> tasks) {
                recyclerViewAdapter.addItems(tasks);
            }
        });
    }
    /**
     * Long clicklistener starts a new task work session for the selected task
     * @param v the selected listitem
     * @return true
     */
    @Override
    public boolean onLongClick(View v) {
        // get the object connected to the item
        Task task = (Task) v.getTag();
        // get reference to the taskPage
        Intent taskIntent = new Intent(this, TaskPage.class);
        // pass the selected task object to it
        taskIntent.putExtra("TASK", task);
        // open activity
        startActivity(taskIntent);
        return true;
    }
    /**
     * Opens hamburger menu window when icon clicked
     * @param item item clicked
     * @return the menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // get items id
        switch (item.getItemId()) {
            // if its our menu icon, open it
            case android.R.id.home:
                frontPageDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
