package com.example.timertask.timertask.Activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.timertask.timertask.R;
import com.example.timertask.timertask.ViewModels.TaskViewModel;
import com.example.timertask.timertask.Models.Task;

/**
 * Here we create a new task
 */
public class LuoTask extends AppCompatActivity {

    /**
     * Button for starting the task
     * TextViews for info about the task (Name&Description)
     */
    private Button aloitaTask;
    private TextView nimi,desc;
    Intent taskPageIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_luo_task);
        Toolbar topToolBar = (Toolbar) findViewById(R.id.topbar);
        aloitaTask = (Button) findViewById(R.id.aloitaTask);
        taskPageIntent = new Intent(this, TaskPage.class);
        nimi = findViewById(R.id.taskNimi);
        desc = findViewById(R.id.taskKuvaus);
        topToolBar.setTitle("Luo Task");
        setSupportActionBar(topToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        aloitaTask.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                aloita();
            }
        });
        //Reset TextView Color back to original
        nimi.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                nimi.setBackgroundColor(Color.GRAY);
            }
        });
        desc.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                desc.setBackgroundColor(Color.GRAY);
            }
        });
    }
    /**
     * Top action  bar handling
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Handlaa topbar actionit
        if(item.getItemId() == android.R.id.home)
            finish();

        return true;
    }
    /**
     * Called when task start Button is pressed
     */
    private void aloita()
    {
        if(nimi.getText().toString().equals("") || desc.getText().toString().equals(""))
        {
            /*
             * Check if fields are empty, in that case alert the user to fill them out
             */
            if(nimi.getText().toString().equals("") && desc.getText().toString().equals("")) {
                nimi.setBackgroundColor(Color.RED);
                desc.setBackgroundColor(Color.RED);
            }
            else if(nimi.getText().toString().equals(""))
            {
                nimi.setBackgroundColor(Color.RED);
            }
            else if(desc.getText().toString().equals(""))
            {
                desc.setBackgroundColor(Color.RED);
            }

        }
        else
        {  /*
            * The user has input some info so we handle the info and start up the task.
            */
            //Proceed
            TaskViewModel taskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
            Task newTask = new Task(nimi.getText().toString(), desc.getText().toString());
            long taskId = -1;
            try {
                // lets create a new task and get its  id
                taskId = taskViewModel.createTask(newTask);
                newTask.setId((int)taskId);
            } catch(Exception ex) {
                ex.printStackTrace();
            }
            // Pass our created task object to the next activity
            taskPageIntent.putExtra("TASK", newTask);
            startActivity(taskPageIntent);
        }
    }
}
