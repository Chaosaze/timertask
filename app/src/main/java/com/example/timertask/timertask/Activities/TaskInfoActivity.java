package com.example.timertask.timertask.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import android.util.Log;

import com.example.timertask.timertask.Adapters.NoteViewAdapter;
import com.example.timertask.timertask.R;
import com.example.timertask.timertask.ViewModels.NoteViewModel;
import com.example.timertask.timertask.Models.Note;
import com.example.timertask.timertask.Models.Task;

/**
 * This activity presents all the data of a specific Task
 * TODO: Listaus kaikista noteista, kuvista ja äänitteistä
 */
public class TaskInfoActivity extends AppCompatActivity {

    private static final String TAG = "TaskInfoActivity";
    Task selectedTask;
    RecyclerView noteList;
    NoteViewModel noteViewModel;
    NoteViewAdapter noteAdapter;
    // audio mediaplayer
    private MediaPlayer mPlayer = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.taskinfo_layout);
        // get task object passed from previous activity
        selectedTask = (Task) getIntent().getSerializableExtra("TASK");
        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        Toolbar toolbar = findViewById(R.id.header);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // set the value of the toolbar to our tasks name and time finished IF it is finished
        if(selectedTask.getFinishedAt() != null)
            getSupportActionBar().setTitle(selectedTask.getName() + " " + new SimpleDateFormat("dd.MM.yyyy").format(selectedTask.getFinishedAt()));
        else
            getSupportActionBar().setTitle(selectedTask.getName());

        setTaskInfo();
        fillRecycleView();
    }

    /**
     * Init and fill the recycleview
     */
    public void fillRecycleView() {
        noteList = this.findViewById(R.id.notesList);

        // init adapter
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        noteAdapter = new NoteViewAdapter(new ArrayList<Note>());
        // get notes and fill the list
        noteViewModel.getNotesByTaskId(selectedTask.getId()).observe(this, new Observer<List<Note>>(){
            @Override
            public void onChanged(@Nullable List<Note> notes)
            {
                noteAdapter.addItems(notes);
            }
        });

        // set adapter and layout manager to the list element
        noteList.setLayoutManager(mLayoutManager);
        noteList.setAdapter(noteAdapter);
    }

    /**
     * Set the info textview values to task values
     */
    public void setTaskInfo() {
        TextView cDate = this.findViewById(R.id.creationDate);
        TextView tUsed = this.findViewById(R.id.timeUsed);
        TextView tDesc = this.findViewById(R.id.desc);
        // Set the date creation date in the toolbar
        cDate.setText(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(selectedTask.getCreatedAt()));
        // create a new date object out of milliseconds
        Date time = new Date(selectedTask.getTimeUsed());
        // format the time to a string
        String timeString = new SimpleDateFormat("HH:mm:ss").format(time);
        tUsed.setText(timeString);
        tDesc.setText((selectedTask.getDescription()));
    }
    /**
     * Top action  bar handling
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // if the toolbar arrow is pressed, return to the previous activity
        if(item.getItemId() == android.R.id.home) {
            switch((String)(getIntent().getSerializableExtra("Activity"))) {
                case "FrontPage":
                    startActivity(new Intent(TaskInfoActivity.this, FrontPage.class));
                    break;
                case "CompletedTasks":
                    startActivity(new Intent(TaskInfoActivity.this, CompletedTasks.class));
                    break;
            }
        }
        return true;
    }

    /**
     * Opens the given image in the default imageviewer
     * @param filePath image filepath
     */
    public void showImage(String filePath) {
        // get a reference to the file with the filepath
        File file = new File(filePath);

        // Generate URI from the file
        Uri photoURI = null;
        // after sdk 24 we have to use fileprovider to create the uri or it will cause an FileUriExposedException
        if (Build.VERSION.SDK_INT >= 24)
            photoURI = FileProvider.getUriForFile(TaskInfoActivity.this, "com.example.timertask.timertask.fileprovider", file);
        else
            photoURI = Uri.fromFile(file);
        // init our imageviewer intent
        Intent intent = new Intent(Intent.ACTION_VIEW, photoURI);
        // Set this flag to allow the intent to read this URI
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
    }

    /**
     * Plays audio file defined by filepath
     * @param filePath String absolute filepath to audio file
     */
    private void startPlaying(String filePath) {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(filePath);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e("TaskInfo", "prepare() failed");
        }
    }

    /**
     * Stops the mediaplayer from playing the audio
     */
    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

}
