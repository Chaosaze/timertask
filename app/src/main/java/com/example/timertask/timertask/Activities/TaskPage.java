package com.example.timertask.timertask.Activities;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.RadioGroup;

import com.example.timertask.timertask.MuistiinpanoDialog;
import com.example.timertask.timertask.R;
import com.example.timertask.timertask.ViewModels.NoteViewModel;
import com.example.timertask.timertask.ViewModels.TaskViewModel;
import com.example.timertask.timertask.ViewModels.WorkDoneViewModel;
import com.example.timertask.timertask.Models.Note;
import com.example.timertask.timertask.Models.Task;
import com.example.timertask.timertask.Models.WorkDone;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Here is the functionality for an ongoing task. Like take picture or do a recording.
 * This shows the timer, where notes can be added, pictures taken and recordings done.
 */
public class TaskPage extends AppCompatActivity {

    // constant variables, used to identify result of the intent
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int WRITE_REQUEST_CODE = 2;

    // directory references
    File pictureDir = null;
    File audioDir = null;

    // Audio recording constants
    private static final String LOG_TAG = "AudioRecordTest";
    private static String mLastAudioFileName = null;
    private static boolean recording = false;

    private MediaRecorder mRecorder = null;

    // Instances of the object regarding current task and its task instance
    private WorkDone currentWorkInstance;
    private Task currentTask;

    // ViewModels
    private NoteViewModel noteViewModel;
    private WorkDoneViewModel workDoneViewModel;

    // Constants for requesting permission for file writing and audio
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    // chronometer instances which are used as stopwatches
    static Chronometer currentDuration;
    static Chronometer totalDuration;
    // mills, reference to the time elapsed for current task.
    // can be handled in the listener, so might not be required
    static long currentTaskTimeElapsed = 0;
    /**
     * Inits our view, and asks for write permissions
     * if users android version is newer than Nougat
     * inits chronometers and adds onclicklisteners
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_page);
        // title bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        // reads the data passed from previous activity
        currentTask = (Task) getIntent().getSerializableExtra("TASK");
        setSupportActionBar(toolbar);
        // set the toolbars value to our tasks name
        getSupportActionBar().setTitle(currentTask.getName());

        // request for required permissions from the user
        requestPermissions();

        // the default picture and audio directory for the app
        pictureDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/TaskTimer/" + currentTask.getId() + "_" + currentTask.getName());
        audioDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PODCASTS + "/TaskTimer/" + currentTask.getId() + "_" + currentTask.getName());
        // if they dont exist, create them
        if(!pictureDir.exists()) pictureDir.mkdirs();
        if(!audioDir.exists()) audioDir.mkdirs();

        // init chronos
        initChronometers();
        // add onclick listeners
        addButtonOnClickListeners();

        // init viewmodels
        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        workDoneViewModel = ViewModelProviders.of(this).get(WorkDoneViewModel.class);

        // create new work(task) instance
        currentWorkInstance = new WorkDone(currentTask.getId());
        try {
            // set the newly created workdone instances id to our reference of it
            currentWorkInstance.setId(workDoneViewModel.createWorkDone(currentWorkInstance).intValue());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Adds onclick listeners to all of our buttons in this activity
     */
    public void addButtonOnClickListeners() {
        // index of "finished" -radiobuttons
        final int FINISHED = 1;
        final int UNFINISHED = 2;

        // get button references
        // button for recording audio
        final Button audioButton = (Button) this.findViewById(R.id.audio);
        // button for ending the current session
        final Button endButton = (Button) this.findViewById(R.id.endTask);
        // button for adding text notes
        final Button textButton = (Button) this.findViewById(R.id.text);
        // Button for taking images
        final Button cameraButton = (Button) this.findViewById(R.id.camera);
        // Radiogroup including 2 radiobuttons, yes and no. Describes if the Task is finished for good
        final RadioGroup finished = (RadioGroup) this.findViewById(R.id.isFinished);
        // set the "no" button checked by default
        finished.check(finished.getChildAt(UNFINISHED).getId());

        // set all onClickListeners
        /**
         * Add onclick listener to the take picture button
         */
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // start the image taking process
                dispatchTakePictureIntent();
            }
        });
        /**
         * Here we add onclick listener to the audio record button
         */
        audioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // record audio
                if (!recording) {
                    if (startRecording()) {
                        audioButton.setBackgroundColor(getResources().getColor(R.color.disable));
                        audioButton.setText("Lopeta");
                        recording = true;
                    }
                } else {
                    stopRecording();
                    audioButton.setBackgroundResource(android.R.drawable.btn_default);
                    audioButton.setText("Nauhoita");
                    // REMOVE!
                    File audio = new File(audioDir.getAbsolutePath() + mLastAudioFileName);
                    recording = false;
                }
            }
        });
        /**
         * Add onclick listener to note taking panel
         */
        textButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open dialog window
                openmd();
            }
        });
        endButton.setOnClickListener(new View.OnClickListener() {
            // this handler should take care of invoking the service and saving our data to db
            // and afterwards going back to the front screen
            @Override
            public void onClick(View v) {
                // stop the chrono meters
                currentDuration.stop();
                totalDuration.stop();
                // by default the base of the chronometer is SystemClock.elapsedRealtime().
                // so by substracting the current SystemClock.elapsedRealtime() and chronos base we get the time elapsed
                currentTaskTimeElapsed = SystemClock.elapsedRealtime() - currentDuration.getBase();
                // update the object and db
                currentTask.setTimeUsed(currentTask.getTimeUsed() + currentTaskTimeElapsed);
                currentTask.setUpdatedAt(new Date());
                // if the task is now finished
                if (finished.indexOfChild(finished.findViewById(finished.getCheckedRadioButtonId())) == FINISHED) {
                    // set the boolean tag to finished, and add the finished timeStamp
                    currentTask.setFinished(true);
                    currentTask.setFinishedAt(currentTask.getUpdatedAt());
                }
                // get reference to our viewmodel (storage service)
                TaskViewModel taskViewModel = ViewModelProviders.of(TaskPage.this).get(TaskViewModel.class);
                // update the current task
                taskViewModel.updateTask(currentTask);
                // set the time spent during this session to the work instance
                currentWorkInstance.setTimeUsed(currentTaskTimeElapsed);
                // update the instances info
                workDoneViewModel.updateWorkDone(currentWorkInstance);

                // Go back to frontpage
                startActivity(new Intent(TaskPage.this, FrontPage.class));
            }
        });
    }
    /*
     * Opens text memo dialog
     */
    public void openmd()
    {
        // create a reference to the dialog
        final MuistiinpanoDialog md = new MuistiinpanoDialog(this, R.layout.muistiinpano_layout);
        // show the dialog
        md.show();
        // When the dialog is closed
        md.mAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                // if the fields are not empty
                if(md.otsikko != null && md.sisalto != null)
                {
                    // Lets save our newly created text note to db
                    Note note = new Note(md.otsikko, md.sisalto, null, null, currentWorkInstance.getId());
                    noteViewModel.createNote(note);
                }
            }
        });
    }
    /**
     * Initialize and start our chronometers
     */
    public void initChronometers() {
        // init the current tasks session stopwatch
        currentDuration = (Chronometer) findViewById(R.id.currentDuration);
        // start it
        currentDuration.start();
        // init the totalDuration stopwatch (time used for the task overall)
        totalDuration = (Chronometer) findViewById(R.id.totalDuration);
        // set the base time of the stopwatch to the total time used for this task
        // by using the relative time SystemClock.elapsedRealtime() and substracting our total time from it
        // which sets us "to the past", and there for setting the timer value to our total duration
        totalDuration.setBase(SystemClock.elapsedRealtime() - currentTask.getTimeUsed() );
        totalDuration.setFormat("Total: %s");
        totalDuration.start();
    }
    /**
     * Opens the androids default device camera to take an picture,
     * saves the image to given directory and calls media scanner to save it
     */
    private void dispatchTakePictureIntent() {
        // init camera intent
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try{
                File file =  createImageFile();
                // scan the temp file
                scanMedia(file.getAbsolutePath());
                // save the note to db
                Note note = new Note(file.getName(), null , file.getAbsolutePath(), null, currentWorkInstance.getId());
                noteViewModel.createNote(note);

                // create uri for the image and pass it to the camera intent
                Uri photoUri = FileProvider.getUriForFile(TaskPage.this, "com.example.timertask.timertask.fileprovider", file);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

                // open camera app with our custom request code
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            } catch(Exception ex) {
                ex.printStackTrace();
            }

        }
    }

    /**
     * When an external activity returns its result, handle it
     * @param requestCode Requestcode given to the activity
     * @param resultCode Result of the operation
     * @param data Data passed from the activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check if the activity operation was successful
        switch(resultCode) {
            case RESULT_OK:
                // if yes, check which operation was executed
                switch (requestCode) {
                    // camera activity result returned
                    case REQUEST_TAKE_PHOTO:
                        break;
                }
                break;
        }
    }

    /**
     * Creates an temp file
     * @return Temp File object
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name from timestamp
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        String imageFileName = timeStamp + "_";
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
               pictureDir      /* directory */
        );
        return image;
    }
    /**
     * Requests for permissions required by the application on runtime
     */
    private void requestPermissions() {
        // if permission is not  granted for audio recording or file writing
        if (ContextCompat.checkSelfPermission(TaskPage.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(TaskPage.this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED  ) {

            // Should we show an explanation of why we need these permissions?
            if (ActivityCompat.shouldShowRequestPermissionRationale(TaskPage.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permissions.
                // if user has newer android version than nougat, ask for permissions (required for sdk version 23+)
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    requestPermissions(permissions, WRITE_REQUEST_CODE);
                }
            }
        }
    }
    /**
     * Instantiate audio recorder settings and start the recording
     */
    private boolean startRecording() {
        mRecorder = new MediaRecorder();
        // generate file path + name with already created directory and timestamp
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        mLastAudioFileName = "/MP3_" + timeStamp + ".mp3";
        // set recorder settings
        try {
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setOutputFile(audioDir.getAbsolutePath() + mLastAudioFileName);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.prepare();
            mRecorder.start();
            return true;
        } catch (IOException e) {
            Log.e("TaskPage", "recorder crashed: ",e);
            return false;
        }
    }
    /**
     * Stops the audio recording
     */
    private void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        // add the new recording as a note to the db with its filepath
        Note note = new Note( mLastAudioFileName, null,null, audioDir.getAbsolutePath() + mLastAudioFileName, currentWorkInstance.getId());
        noteViewModel.createNote(note);
        scanMedia(audioDir.getAbsolutePath() + mLastAudioFileName);
    }

    /**
     * Scans the given file so it's visible in GALLERY
     * @param path
     */
    private void scanMedia(String path) {
        // get file reference based on our newly created images file path
        File f = new File(path);
        // generate uri
        Uri uri = Uri.fromFile(f);
        // init scanner intent
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
        // save it to gallery
        this.sendBroadcast(mediaScanIntent);

    }
}
