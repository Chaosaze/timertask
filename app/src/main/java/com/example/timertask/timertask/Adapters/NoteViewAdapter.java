package com.example.timertask.timertask.Adapters;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.timertask.timertask.MuistiinpanoDialog;
import com.example.timertask.timertask.R;
import com.example.timertask.timertask.Models.Note;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class NoteViewAdapter extends RecyclerView.Adapter<NoteViewAdapter.RecyclerViewHolder> {

    private List<Note> noteList;

    public NoteViewAdapter(List<Note> notes)
    {
        noteList = notes;
    }

    @Override
    public NoteViewAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NoteViewAdapter.RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item, parent, false));
    }

    @Override
    public void onBindViewHolder(NoteViewAdapter.RecyclerViewHolder holder, int position) {
        Note note = noteList.get(position);

        holder.noteName.setText(note.getTitle());
        holder.itemView.setTag(note);

        // set the interact buttons icon based on the note type
        if (note.getAudioPath() != null)
            holder.interactButton.setImageResource(android.R.drawable.ic_btn_speak_now);
        else if (note.getPicPath() != null)
            holder.interactButton.setImageResource(android.R.drawable.ic_menu_gallery);
        else
            holder.interactButton.setImageResource(android.R.drawable.ic_menu_edit);
    }

    /**
     * Overriding the basic adapter get item count.
     * @return Amount of items in list
     */
    @Override
    public int getItemCount() { return noteList.size(); }

    /**
     * Add items to recyclerview
     * @param notes List of tasks to be added
     */
    public void addItems(List<Note> notes) {
        this.noteList = notes;

        notifyDataSetChanged();
    }

    //Holder luokka
    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView noteName;
        public ImageButton interactButton;
        // keep track of the note which audio is playing
        public static int activeNoteId = -1;
        // audio mediaplayer
        public static MediaPlayer mPlayer;

        /**
         * Init our items inside recyclerview
         * @param view The view which contains our specific list items
         */
        RecyclerViewHolder(View view) {
            super(view);
            // get reference to our note items
            noteName = view.findViewById(R.id.noteName);
            interactButton = view.findViewById(R.id.interact);
            // set the onclicklistener to our interact button based on type of note
            interactButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Note note = (Note) ((View)(v.getParent())).getTag();

                    if (note.getText() != null) {
                        // // if the note type is text, open dialog with our data
                        MuistiinpanoDialog dialog = new MuistiinpanoDialog(v.getContext(), R.layout.muistiinpano_layout, note.getTitle(), note.getText());
                        // lets deny edition of the edittext fields
                        dialog.otsikkoTxt.setFilters(new InputFilter[] {
                                new InputFilter() {
                                    public CharSequence filter(CharSequence src, int start, int end, Spanned dst, int dstart, int dend) {
                                        return src.length() < 1 ? dst.subSequence(dstart, dend) : "";
                                    }
                                }
                        });
                        dialog.txt.setFilters(new InputFilter[] {
                                new InputFilter() {
                                    public CharSequence filter(CharSequence src, int start, int end, Spanned dst, int dstart, int dend) {
                                        return src.length() < 1 ? dst.subSequence(dstart, dend) : "";
                                    }
                                }
                        });
                        dialog.show();
                    } else if (note.getPicPath() != null)
                        // if type is image, open it
                        showImage(note.getPicPath(), v.getContext());
                    else {
                        // else its an audio file
                        // if we aren't currently playing, play the audio
                        if (activeNoteId == -1) {
                            activeNoteId = note.getId();
                            startPlaying(note.getAudioPath());
                        } else {
                            // if we are, stop the player
                            stopPlaying();
                            // if note which audio we are playing is not the same as the note which audio we want to play
                            if (activeNoteId != note.getId()) {
                                // play the audio and set the current id reference to this id
                                startPlaying(note.getAudioPath());
                                activeNoteId = note.getId();
                            } else // else set the active id to -1 (no audio playing) again
                                activeNoteId = -1;
                        }
                    }
                }
            });
        }
        /**
         * Opens the given image in the default imageviewer
         * @param filePath image filepath
         */
        public void showImage(String filePath, Context context) {
            // get a reference to the file with the filepath
            File file = new File(filePath);

            // Generate URI from the file
            Uri photoURI = null;
            // after sdk 24 we have to use fileprovider to create the uri or it will cause an FileUriExposedException
            if (Build.VERSION.SDK_INT >= 24)
                photoURI = FileProvider.getUriForFile(context, "com.example.timertask.timertask.fileprovider", file);
            else
                photoURI = Uri.fromFile(file);
            // init our imageviewer intent
            Intent intent = new Intent(Intent.ACTION_VIEW, photoURI);
            // Set this flag to allow the intent to read this URI
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(intent);
        }

        /**
         * Plays audio file defined by filepath
         * @param filePath String absolute filepath to audio file
         */
        private void startPlaying(String filePath) {
            mPlayer = new MediaPlayer();
            try {
                mPlayer.setDataSource(filePath);
                mPlayer.prepare();
                mPlayer.start();
            } catch (IOException e) {
                Log.e("NoteAdapter", "prepare() failed");
            }
        }

        /**
         * Stops the mediaplayer from playing the audio
         */
        private void stopPlaying() {
            mPlayer.release();
            mPlayer = null;
        }
    }
}
