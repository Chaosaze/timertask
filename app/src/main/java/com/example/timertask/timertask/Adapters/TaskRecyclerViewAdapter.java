package com.example.timertask.timertask.Adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.timertask.timertask.R;
import com.example.timertask.timertask.Activities.TaskInfoActivity;
import com.example.timertask.timertask.Activities.TaskPage;
import com.example.timertask.timertask.Models.Task;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 *  Here we connect the view with the data
 */
public class TaskRecyclerViewAdapter extends RecyclerView.Adapter<TaskRecyclerViewAdapter.RecyclerViewHolder> {
    private List<Task> taskList;
    private View.OnLongClickListener longClickListener;
    public static String callerClass;

    public TaskRecyclerViewAdapter (List<Task> taskList, View.OnLongClickListener longClickListener, String callerClass) {
        this.taskList = taskList;
        this.longClickListener = longClickListener;
        this.callerClass = callerClass;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Task task = taskList.get(position);
        holder.nameTextView.setText(task.getId() + " "+ task.getName());
        holder.dateTextView.setText(new SimpleDateFormat("dd.MM.YYYY").format(task.getCreatedAt()));
        holder.itemView.setTag(task);
        holder.itemView.setOnLongClickListener(longClickListener);
        // if the task is completed, hide the interact button
        if (task.isFinished()) {
            holder.interactButton.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Overriding the basic adapter get item count.
     * @return Amount of items in list
     */
    @Override
    public int getItemCount() {
        return taskList.size();
    }

    /**
     * Add items to recyclerview
     * @param tasks List of tasks to be added
     */
    public void addItems(List<Task> tasks) {
        this.taskList = tasks;
        notifyDataSetChanged();
    }

    /**
     * Configures our items inside the recyclerview
     */
    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private TextView dateTextView;
        private ImageButton interactButton;
        private ImageButton infoButton;
        private ImageButton delButton;

        /**
         * Init our items inside recyclerview
         * @param view The view which contains our specific list items
         */
        RecyclerViewHolder(View view) {
            super(view);
            // get references to our items in 1 list item
            nameTextView = view.findViewById(R.id.taskName);
            dateTextView =  view.findViewById(R.id.taskDate);

            interactButton = view.findViewById(R.id.interact);
            //delButton =  view.findViewById(R.id.deleteTask);
            infoButton = view.findViewById(R.id.taskInfo);

            //Set on click listeners
            interactButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Task task = (Task) ((View) v.getParent()).getTag();
                    Intent taskIntent = new Intent(v.getContext(), TaskPage.class);
                    // pass the selected task object to it
                    taskIntent.putExtra("TASK", task);
                    // open activity
                    v.getContext().startActivity(taskIntent);

                }
            });
            infoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Task task = (Task) ((View) v.getParent()).getTag();
                    Intent taskInfoIntent = new Intent(v.getContext(), TaskInfoActivity.class);
                    taskInfoIntent.putExtra("Activity", callerClass);
                    taskInfoIntent.putExtra("TASK", task);
                    v.getContext().startActivity(taskInfoIntent);
                }
            });
}


            /*
            delButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            */
        }
    }
    /*
    public View getView(final int position, View convertView, ViewGroup parent) {
        interactButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //...
            }
        });
    }
    */
