package com.example.timertask.timertask;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.example.timertask.timertask.Daos.*;
import com.example.timertask.timertask.Models.*;

/**
 * Handles the connection to the database
 *
 */
@Database(entities = {Task.class, Note.class, WorkDone.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    // create an interface for our Model daos
    public abstract TaskDao taskDao();
    public abstract NoteDao noteDao();
    public abstract WorkDoneDao workDoneDao();

    private static volatile AppDatabase INSTANCE;
    // Database instances an be heavy, so lets use singleton pattern
    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                // if db instance hasnt been created, create it
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "task_time_db").build();
                }
            }
        }
        return INSTANCE;
    }
}
