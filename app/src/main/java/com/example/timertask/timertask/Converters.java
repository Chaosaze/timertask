package com.example.timertask.timertask;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;


/**
 * Converters for our Room persistence library Daos
 */
public class Converters {
    /**
     * From long value to Date object
     * @param value time as long vlaue
     * @return Date object of the time
     */
    @TypeConverter
    public Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    /**
     * From date object to Long
     * @param date time as a date object
     * @return Time as long
     */
    @TypeConverter
    public Long dateToTimestamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }
}
