package com.example.timertask.timertask.Daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.timertask.timertask.Models.Note;

import java.util.List;

@Dao
public interface NoteDao {

    @Query("SELECT * FROM notes WHERE id LIKE :id")
    LiveData<Note> getNoteById(int id);

    @Query("SELECT * FROM notes WHERE work_done_id LIKE :id")
    LiveData<List<Note>> getNotesByWorkId(int id);

    @Query("SELECT * FROM notes WHERE work_done_id IN (SELECT id FROM work_done WHERE task_id LIKE :id )")
    LiveData<List<Note>> getNotesByTaskId(int id);

    @Insert
    void createNote(Note note);

    @Delete
    void deleteNote(Note note);
}
