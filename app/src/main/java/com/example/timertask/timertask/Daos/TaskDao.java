package com.example.timertask.timertask.Daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.timertask.timertask.Models.Task;

import java.util.List;

/**
 *  Database query to update task list
 */
@Dao
public interface TaskDao {

    // get Task by its id
    @Query("SELECT * FROM tasks WHERE id LIKE :id")
    LiveData<Task> getTaskById(int id);

    // get all tasks ordered by lastest updated
    @Query("SELECT * FROM tasks ORDER BY updated_at DESC")
    LiveData<List<Task>> getTasks();

    // get all tasks which are finished, ordered by latest updated
    @Query("SELECT * FROM tasks WHERE finished LIKE 1 ORDER BY updated_at DESC ")
    LiveData<List<Task>> getFinishedTasks();

    // get all tasks which are unfinished, ordered by latest updated
    @Query("SELECT * FROM tasks WHERE finished LIKE 0 ORDER BY updated_at DESC ")
    LiveData<List<Task>> getUnFinishedTasks();

    // create task and return its id
    @Insert()
    long createTask(Task task);

    // delete task
    @Delete
    void deleteTask(Task task);

    // update existing task
    @Update
    void updateTask(Task task);

}
