package com.example.timertask.timertask.Daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.timertask.timertask.Models.WorkDone;

import java.util.List;

@Dao
public interface WorkDoneDao {

    @Query("SELECT * FROM work_done WHERE id LIKE :id")
    LiveData<WorkDone> getWorkDoneById(int id);

    @Query("SELECT * FROM work_done WHERE task_id LIKE :id")
    LiveData<List<WorkDone>> getWorkDoneByTaskId(int id);

    @Insert
    Long createWorkDone(WorkDone workDone);

    @Update
    void updateWorkDone(WorkDone workDone);

    @Delete
    void deleteWorkDone(WorkDone workDone);
}
