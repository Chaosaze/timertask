package com.example.timertask.timertask.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Note Model definition
 */
@Entity(tableName = "notes")
public class Note {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    // set foreignKey workDoneId to the work done tables id column, and remove this entry if the id from tasks is removed
    @ForeignKey(entity = WorkDone.class, onDelete = ForeignKey.CASCADE, parentColumns = "id", childColumns = "workDoneId")
    @ColumnInfo(name = "work_done_id")
    @NonNull
    private int workDoneId;

    private String title;
    private String text;
    @ColumnInfo(name = "pic_path")
    private String picPath;
    @ColumnInfo(name = "audio_path")
    private String audioPath;

    public Note(String title, String text, String picPath, String audioPath, int workDoneId) {
        setTitle(title);
        setText(text);
        setPicPath(picPath);
        setAudioPath(audioPath);
        setWorkDoneId(workDoneId);
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWorkDoneId() {
        return workDoneId;
    }

    public void setWorkDoneId(int workDoneId) {
        this.workDoneId = workDoneId;
    }
}
