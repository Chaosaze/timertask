package com.example.timertask.timertask.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;

/**
 * Task model
 */
@Entity(tableName = "tasks")
public class Task implements Serializable{
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    private String name;

    private String description;
    private boolean finished = false;
    @ColumnInfo(name = "created_at")
    private Date createdAt;
    @ColumnInfo(name = "finished_at")
    private Date finishedAt;
    @ColumnInfo(name = "updated_at")
    private Date updatedAt;
    @ColumnInfo(name = "time_used")
    private long timeUsed = 0;// or timeUsed;
    @Ignore
    public Task(String name, String description) {
        this(name, description, new Date());
    }
    public Task(String name, String description, Date createdAt) {
        setName(name);
        setFinished(false);
        setCreatedAt(createdAt);
        setUpdatedAt(createdAt);
        setDescription(description);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Date finishedAt) {
        this.finishedAt = finishedAt;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getTimeUsed() {
        return timeUsed;
    }

    public void setTimeUsed(long timeUsed) {
        this.timeUsed = timeUsed;
    }

}
