package com.example.timertask.timertask.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

/**
 * Task instance model
 */
@Entity(tableName = "work_done")
public class WorkDone {
    @PrimaryKey(autoGenerate = true)
    private int id;
    // set foreignKey taskId to the Task tables id column, and remove this entry if the id from tasks is removed
    @ForeignKey(entity = Task.class, onDelete = ForeignKey.CASCADE, parentColumns = "id", childColumns = "taskId")
    @ColumnInfo(name = "task_id")
    private int taskId;
    @ColumnInfo(name = "created_at")
    private Date createdAt;
    @ColumnInfo(name = "finished_at")
    private Date finishedAt;
    @ColumnInfo(name = "time_used")
    private long timeUsed;
    @Ignore
    public WorkDone(int taskId) {
        this(taskId, new Date());
    }
    public WorkDone(int taskId, Date createdAt ) {
        setTaskId(taskId);
        setCreatedAt(createdAt);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Date finishedAt) {
        this.finishedAt = finishedAt;
    }

    public long getTimeUsed() {
        return timeUsed;
    }

    public void setTimeUsed(long timeUsed) {
        this.timeUsed = timeUsed;
    }
}
