package com.example.timertask.timertask;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Here we show a note view dialog panel with a textView
 * Tarkoitus näyttää muistiinpano dialog ikkuna jossa textView
 */

public class MuistiinpanoDialog extends AlertDialog.Builder {
    private Context mContext;
    public AlertDialog mAlertDialog;
    private int dialogLayoutResource;
    private View dialogView;
    public String sisalto, otsikko;
    public EditText txt, otsikkoTxt;

    // inits an empty dialog
    public MuistiinpanoDialog(Context context, int l) {
        this(context, l, null, null);
    }
    // inits an empty or filled dialog
    public MuistiinpanoDialog(Context context, int l, String otsikko, String sisalto) {
        super(context);
        mContext = context;
        dialogLayoutResource = l;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        dialogView = inflater.inflate(dialogLayoutResource, null);

        txt = dialogView.findViewById(R.id.sisalto);
        otsikkoTxt = dialogView.findViewById(R.id.otsikko);
        txt.setText(sisalto, TextView.BufferType.EDITABLE);
        otsikkoTxt.setText(otsikko,  TextView.BufferType.EDITABLE);
    }

    /**
     * Opens the dialog window
     * @return the dialog
     */
    @SuppressLint("InflateParams")
    @Override
    public AlertDialog show() {
        setTitle("Muistiinpano");
        setCancelable(true);

        setView(dialogView);
        if (otsikkoTxt.getText() == null && txt.getText() == null) {
            setPositiveButton("Valmis", dialogClickListener);
            setNegativeButton("Peruuta", dialogClickListener);
        } else {
            setPositiveButton("Sulje", dialogClickListener);
        }

        mAlertDialog = super.show();
        return mAlertDialog;
    }
    // Save the data given and close the dialog
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            //Just dismiss because logic is done on the calling side
            otsikko = otsikkoTxt.getText().toString();
            sisalto = txt.getText().toString();
            mAlertDialog.dismiss();
        }
    };

}

