package com.example.timertask.timertask.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.timertask.timertask.AppDatabase;
import com.example.timertask.timertask.Models.Note;

import java.util.List;

/**
 * Viewmodel interface for our Note data. Used to access the data in sql database asynchronously
 */
public class NoteViewModel extends AndroidViewModel {
    private AppDatabase database;
    // init db
    public NoteViewModel(Application application) {
        super(application);
        database = AppDatabase.getDatabase(this.getApplication());
    }

    /**
     * Get notes by task work instance id
     * @param id work id
     * @return List of notes
     */
    public LiveData<List<Note>> getNotesByWorkId(int id) {
        return database.noteDao().getNotesByWorkId(id);
    }

    /**
     * Get notes for specific task
     * @param id Task id
     * @return List of notes
     */
    public LiveData<List<Note>> getNotesByTaskId(int id) {
        return database.noteDao().getNotesByTaskId(id);
    }

    /**
     * Get a specific note by its id
     * @param id Note id
     * @return Note
     */
    public LiveData<Note> getNoteById(int id) {
        return database.noteDao().getNoteById(id);
    }

    /**
     * Create a new note
     * @param note Note to be added to db
     */
    public void createNote(Note note) {
        new CreateNoteAsyncTask(database).execute(note);
    }
    /**
     * TODO updateNote(Note note)
     * TODO deletenote(Note note)
     */


    /**
     * Asynctask, responsible of adding new notes to the db
     */
    private static class CreateNoteAsyncTask extends AsyncTask<Note, Void, Void> {

        private AppDatabase db;

        // init db
        CreateNoteAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        /**
         * Create a new note in the db
         *
         * @param params Notes
         * @return null
         */
        @Override
        protected Void doInBackground(final Note... params) {
            db.noteDao().createNote(params[0]);
            return null;
        }
    }
}