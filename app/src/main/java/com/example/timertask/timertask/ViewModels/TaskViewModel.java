package com.example.timertask.timertask.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.timertask.timertask.AppDatabase;
import com.example.timertask.timertask.Models.Task;

import java.util.List;

/**
 * Viewmodel interface for our Task data. Used to access the data in sql database asynchronously
 */
public class TaskViewModel extends AndroidViewModel {
    private AppDatabase database;

    public TaskViewModel(Application application){
        super(application);

        database = AppDatabase.getDatabase(this.getApplication());
    }

    /**
     * Gets all tasks
     * @return List of tasks
     */
    public LiveData<List<Task>> getTasks() {
        return database.taskDao().getTasks();
    }

    /**
     * Get all finished tasks
     * @return List of finished tasks
     */
    public LiveData<List<Task>> getFinishedTasks() {
        return database.taskDao().getFinishedTasks();
    }
    /**
     * Get all unfinished tasks
     * @return List of unfinished tasks
     */
    public LiveData<List<Task>> getUnFinishedTasks() {
        return database.taskDao().getUnFinishedTasks();
    }

    /**
     * Delete given Task from db asynchronously
     * @param task Task to be deleted
     */
    public void deleteTask(Task task) {
        new DeleteAsyncTask(database).execute(task);
    }

    /**
     * Create a new task to db asynchronously
     * @param task Task to be created
     */
    public long createTask(Task task) throws java.util.concurrent.ExecutionException, java.lang.InterruptedException{
       AsyncTask<Task,Void, Long> asyncTask =  new AddAsyncTask(database).execute(task);
       return asyncTask.get();
    }
    /**
     * Update existing task asynchronously
     * @param task Task to be updated
     */
    public void updateTask(Task task) {
        new UpdateAsyncTask(database).execute(task);
    }
    /**
     * Asynctask, responsible of deleting items in the db
     */
    private static class DeleteAsyncTask extends AsyncTask<Task, Void, Void> {

        private AppDatabase db;

        DeleteAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }
        /**
         * Delete a task in the db
         * @param params Tasks to be deleted
         * @return null
         */
        @Override
        protected Void doInBackground(final Task... params) {
            db.taskDao().deleteTask(params[0]);
            return null;
        }
    }

    /**
     * Asynctask, responsible of adding new items to the db
     */
    private static class AddAsyncTask extends AsyncTask<Task, Void, Long> {

        private AppDatabase db;
        // init db
        AddAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        /**
         * Create a new task in the db
         * @param params Tasks
         * @return Long task rowid/id
         */
        @Override
        protected Long doInBackground(final Task... params) {return (Long)db.taskDao().createTask(params[0]);}
    }

    /**
     * Asynctask, responsible of adding new items to the db
     */
    private static class UpdateAsyncTask extends AsyncTask<Task, Void, Void> {

        private AppDatabase db;
        // init db
        UpdateAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        /**
         * Update existing task in db
         * @param params Tasks
         * @return null
         */
        @Override
        protected Void doInBackground(final Task... params) {
            db.taskDao().updateTask(params[0]);
            return null;
        }
    }
}
