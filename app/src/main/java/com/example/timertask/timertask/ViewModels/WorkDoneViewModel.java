package com.example.timertask.timertask.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.timertask.timertask.AppDatabase;
import com.example.timertask.timertask.Models.WorkDone;

import java.util.List;

/**
 * Viewmodel interface for our WorkDone data. Used to access the data in sql database asynchronously
 */
public class WorkDoneViewModel extends AndroidViewModel {
    private AppDatabase database;

    public WorkDoneViewModel(Application application) {
        super(application);
        database = AppDatabase.getDatabase(this.getApplication());
    }

    /**
     * Get work instance by its id
     * @param id work instance id
     * @return work instance
     */
    public LiveData<WorkDone> getWorkDoneById(int id) {
        return database.workDoneDao().getWorkDoneById(id);
    }
    /**
     * Get all work instances by task id
     * @param id int task Id
     * @return Livedata List of work instances
     */
    public LiveData<List<WorkDone>> getWorkDonesByTaskId(int id) {
        return database.workDoneDao().getWorkDoneByTaskId(id);
    }

    /**
     * Create now work instance to db
     * @param workDone work done instance
     * @result id of the newly created work instance
     */
    public Long createWorkDone(WorkDone workDone) throws java.util.concurrent.ExecutionException, java.lang.InterruptedException {
        AsyncTask<WorkDone,Void, Long> asyncTask = new CreateWorkDoneAsyncTask(database).execute(workDone);
        return asyncTask.get();
    }
    /**
     * Update existing workdone instance asynchronously
     * @param workDone Workdone to be updated
     */
    public void updateWorkDone(WorkDone workDone) {
        new UpdateWorkDoneAsyncTask(database).execute(workDone);
    }


    /**
     * Asynctask, responsible of adding new work instance to the db
     */
    private static class CreateWorkDoneAsyncTask extends AsyncTask<WorkDone, Void, Long> {

        private AppDatabase db;

        // init db
        CreateWorkDoneAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        /**
         * Create a new work instance in the db
         *
         * @param params Notes
         * @return null
         */
        @Override
        protected Long doInBackground(final WorkDone... params) {
            return db.workDoneDao().createWorkDone(params[0]);
        }
    }
    /**
     * Asynctask, responsible of adding updating existing work instances to the db
     */
    private static class UpdateWorkDoneAsyncTask extends AsyncTask<WorkDone, Void, Void> {

        private AppDatabase db;
        // init db
        UpdateWorkDoneAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        /**
         * Update existing task in db
         * @param params Tasks
         * @return null
         */
        @Override
        protected Void doInBackground(final WorkDone... params) {
            db.workDoneDao().updateWorkDone(params[0]);
            return null;
        }
    }
}
